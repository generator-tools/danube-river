import pint
import csv

# Structure generators
import DanubeRiver.resistors
import DanubeRiver.caps
import DanubeRiver.fet

class DanubeRiverCSV:
	csv = []
	config = None
	ureg = None
	fieldnames = []

	def __init__(self, wafer):
		self.config = wafer.config
		self.ureg = self.config.ureg
		self.wafer = wafer

		header = {}
		header['wafer_name'] = "Wafer"
		header['pattern_id'] = "Pattern ID"
		header['x'] = "X"
		header['y'] = "Y"
		header['probe_config'] = "Probe configuration"
		header['type'] = "Type (resistor/cap/...)"
		header['setup'] = "Test setup/input parameters"
		header['expted_result'] = "Expected results"
		header['layer'] = "Layers"
		header['comment'] = "Comments"

		for field in header:
			self.fieldnames.append(field)

		self.csv.append(header)

	def add_cell(self, cell, pos):
		row = {
			'wafer_name' : self.wafer.dn,
			'pattern_id' : cell.name,
			'x' : pos[0],
			'y' : pos[1],
			'probe_config' : 'Kelvin',
		}

		match type(cell):
			case DanubeRiver.fet.DanubeTestMOSFET:
				row['type'] = "FET"
				row['setup'] = "IDS-VGS"
			case DanubeRiver.resistors.DanubeTestResistor:
				row['type'] = "Resistor"
				row['setup'] = "I-V"
			case DanubeRiver.caps.DanubeTestCapacitor:
				row['type'] = "Capacitor"
				row['setup'] = "C-V"
				row['comment'] = "Area="+str(round(cell.area.m_as(self.ureg.um**2)))+"um^2"

		self.csv.append(row)
	
	def generate_csv(self, outname):
		with open(outname, 'w', newline='') as csvfile:
			writer = csv.DictWriter(csvfile, fieldnames=self.fieldnames)
			for line in self.csv:
				writer.writerow(line)
			csvfile.close()
