from enum import Enum, auto

class DanubeProbeConfiguration(Enum):
	KELVIN = auto()
	DUAL = auto()
	GSG = auto()
	GS = auto()
	SG = auto()

