import gdsfactory as gf
from gdsfactory.components.via_stack import via_stack as via_stack_factory

import pint

import librepdk.helper as lph
from librepdk.types import Direction

from DanubeRiver.types import DanubeProbeConfiguration

class AbstractDanubeStructure(gf.Component):
	parent = None
	config = None
	ureg = None
	min_row_vias = 1
	pads = []

	left_lower_corner = [0,0] # keep track of the moving around

	# for the lowest interconnect
	device_layers = ['nwell','ndiffusion','pdiffusion', 'poly', 'pwell',]

	name = "Test structure"

	def __init__(self, config, name):
		gf.Component.__init__(self, name)

		self.config = config
		self.name = name
		self.ureg = self.config.ureg
		self.pitch = config.get_pad_pitch()

		# the layers:
		self.padlayer_name = config.get_pad_layer() # layer where we put the text
		self.structnamelayer_name = config.get_structure_label_layer() # layer where we put the text
		self.padnumlayer_name = config.get_pad_label_layer() # what label to put the pad numbers onto
		self.conlayer_name = config.get_pad_connection_layer() # layer where the pads go to

		self.padlayer = config.get_layer_id(self.padlayer_name)
		self.structnamelayer = config.get_layer_id(self.structnamelayer_name)
		self.padnumlayer = config.get_layer_id(self.padnumlayer_name)
		self.conlayer = config.get_layer_id(self.conlayer_name)

		# font sizes
		self.nsize = config.get_pad_label_size() # how big are the pad numbers
		self.lsize = config.get_structue_label_size() # how big is the name label of the structure

		# offset
		dimensions = config.get_pad_dimensions()
		self.pad_w = dimensions[0]
		self.pad_h = dimensions[1]

	def get_name(self):
		return self.name

	# pads and interconnect
	def init_pads(self, probe_type, with_metal=False):
		# labeling pads
		fsn = lph.to_nm(self.config, self.nsize) # font size
		pl1 = gf.components.text(text="1", size=fsn, justify="left", layer=self.padnumlayer)
		pl2 = gf.components.text(text="2", size=fsn, justify="left", layer=self.padnumlayer)
		pl3 = gf.components.text(text="3", size=fsn, justify="left", layer=self.padnumlayer)
		pl4 = gf.components.text(text="4", size=fsn, justify="left", layer=self.padnumlayer)

		# labeling structure
		fsl = lph.to_nm(self.config, self.lsize) # font size
		stlabel = gf.components.text(text=self.get_name(), size=fsl, justify="center", layer=self.structnamelayer)
		x = lph.to_nm(self.config, self.nsize*2)
		y = lph.to_nm(self.config, self.lsize/2)
		self << stlabel.move([x,y])

		# rectangle in the lower left corner for zeroing
		rectangle = gf.components.rectangle(size=(fsn,fsl), layer=self.structnamelayer)
		self << rectangle

		# create a pad
		pad = gf.components.pad(size=(lph.to_nm(self.config, self.pad_w), lph.to_nm(self.config, self.pad_h)), layer=self.padlayer)

		# calculate pad locations
		px1 = lph.to_nm(self.config, self.pad_w/2 + self.nsize*2)
		py1 = lph.to_nm(self.config, self.pad_h/2 + self.lsize*2)
		px2 = lph.to_nm(self.config, self.pitch + self.pad_w*1.5 + self.nsize*2)
		py2 = lph.to_nm(self.config, self.pitch + self.pad_h*1.5 + self.lsize*2)

		# calculate pad number locations
		lx1 = 0
		lx2 = lph.to_nm(self.config, self.pitch + 2*self.pad_w + 3*self.nsize)
		ly1 = lph.to_nm(self.config, self.pad_h/2 + self.lsize*2 )
		ly2 = lph.to_nm(self.config, self.pitch + 1.5*self.pad_w + self.lsize*2)

		self.pads = [None, None, None, None]

		# add pads and their labels:
		self.pads[0] = (self << pad.move([px1, py2]))
		self << pl1.move([lx1, ly2])
		self.pads[1] = (self << pad.move([px2, py2]))
		self << pl2.move([lx2, ly2])
		self.pads[2] = (self << pad.move([px2, py1]))
		self << pl3.move([lx2, ly1])
		self.pads[3] = (self << pad.move([px1, py1]))
		self << pl4.move([lx1, ly1])

		# do we need an additional metal?
		if with_metal:
			rectangle = gf.components.rectangle(size=(pad.xsize,pad.ysize), layer=self.conlayer)
			rectangle = rectangle.move(lph.snap_to_grid(self.config, [-pad.xsize/2, -pad.ysize/2]))
			self << rectangle.move([px1, py2])
			self << rectangle.move([px2, py2])
			self << rectangle.move([px2, py1])
			self << rectangle.move([px1, py1])

		llx = self.pad_w + self.nsize*2
		lly = self.pad_h + self.lsize*2

		# keep track of the moving around
		self.left_lower_corner = [llx, lly]

	def connect_quad(self, e1, e2, layer):
		self << gf.routing.route_quad(
			e1,
			e2,
			width1=None,
			width2=None,
			layer=self.config.get_layer_id(layer)
		)

	def connect_rect(self, e1, e2, layer, mode='L', w=None):
		self << gf.routing.route_sharp(
			e1,
			e2,
			width=lph.to_nm(self.config, self.config.get_min_width(layer)) if w is None else w,
			path_type=mode,
			layer=self.config.get_layer_id(layer)
		)

	def connect_pads(self, direction):
		# wire dimensions
		w = lph.to_nm(self.config, self.pitch+2*self.pad_w)
		h = lph.to_nm(self.config, self.pitch+2*self.pad_h)

		# wire 1 offset
		x1 = lph.to_nm(self.config, -self.pad_w/2)
		y1 = lph.to_nm(self.config, -self.pad_h/2)
		# wire 2 offset
		x2 = lph.to_nm(self.config, self.pitch+self.pad_w/2)
		y2 = lph.to_nm(self.config, self.pitch+self.pad_h/2)

		match direction:
			case Direction.VERTICAL:
				self << gf.routing.route_quad(self.pads[0].ports["e2"], self.pads[3].ports["e4"], layer=self.conlayer)
				self << gf.routing.route_quad(self.pads[1].ports["e2"], self.pads[2].ports["e4"], layer=self.conlayer)
			case Direction.HORIZONTAL:
				self << gf.routing.route_quad(self.pads[0].ports["e1"], self.pads[1].ports["e3"], layer=self.conlayer)
				self << gf.routing.route_quad(self.pads[3].ports["e1"], self.pads[2].ports["e3"], layer=self.conlayer)
