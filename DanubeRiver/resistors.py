from scipy import constants
from math import sqrt
from math import floor

import gdsfactory as gf
import numpy as np

import phidl.geometry as pg
import phidl.path as pp

from phidl import Device

from DanubeRiver.structure import AbstractDanubeStructure

from DanubeRiver.types import DanubeProbeConfiguration

from librepdk.types import Direction

from librepdk.resistor import MeanderResistor
from librepdk.resistor import StripResistor

import librepdk.helper as lph

class DanubeTestResistor(AbstractDanubeStructure):
	i0p = i1p = u0p = u1p = 0
	layer = None
	U = None
	Imax = None

	def __init__(self, config, layer, factor, direction):
		AbstractDanubeStructure.__init__(self, config, self.make_name(layer, factor, direction))
		self.init_pads(DanubeProbeConfiguration.KELVIN)

		self.config = config
		self.minimumr = config.get_resistor_minimum_ohm()
		self.Imax = config.get_resistor_max_current()
		self.R = self.minimumr
		self.layer = layer
		self.U = self.Imax * self.R

		if self.check_can_be_straight(layer, factor, direction):
			self.generate_straight_resistor(layer, factor, direction)
		else:
			self.generate_meander_resistor(layer, factor, direction)

	# helper function:
	def make_name(self, layer, factor, direction=None):
		ret = layer.upper()
		ret += "_"

		if direction == Direction.HORIZONTAL:
			ret += "H_"
		elif direction == Direction.VERTICAL:
			ret += "V_"
		else:
			raise Exception("No direction given")

		ret += str(int(factor))
		return ret

	def calculate_resistance(self, layer, length, width):
		sheetrs = self.config.get_resistor_sheetres(layer)
		rx = sheetrs[0]*(length/width)
		ry = sheetrs[1]*(length/width)
		return (rx,ry)

	# meander stuff:
	def normal_meander_resistor(self, res, direction):
		x = self.left_lower_corner[0]
		y = self.left_lower_corner[1]
		x += (self.pitch-res.xsize*self.ureg.nm)/2
		y += (self.pitch-res.ysize*self.ureg.nm)/2
		x = lph.to_nm(self.config, x)
		y = lph.to_nm(self.config, y)
		r = res.move([x,y])
		self << r # placing the resistor
		self.connect_normal_meander_resistor(r, direction) # add wires

	def connect_normal_meander_resistor(self, r, direction):
		# resistor meander structure
		if direction == Direction.HORIZONTAL:
			self.connect_rect(self.pads[3].ports["e2"], r.ports["e1_1"], layer=self.conlayer_name)
			self.connect_rect(self.pads[0].ports["e4"], r.ports["e1_2"], layer=self.conlayer_name)
		elif direction == Direction.VERTICAL:
			self.connect_rect(self.pads[2].ports["e1"], r.ports["e1_1"], layer=self.conlayer_name)
			self.connect_rect(self.pads[3].ports["e3"], r.ports["e1_2"], layer=self.conlayer_name)

	def giant_meander_resistor(self, r, direction):
		width = r.get_wire_width()
		conwidth = self.config.get_min_width(self.conlayer_name)
		conwidth = width if conwidth < width else conwidth
		x = self.xsize+lph.to_nm(self.config, self.nsize)
		y = 0

		if direction == Direction.VERTICAL:
			r = r.rotate(180)
			r = r.move([-r.xmin, -r.ymin])
		r = r.move([x, y])

		cw = lph.to_nm(self.config, conwidth)
		dy = r.ysize-self.ysize
		if direction == Direction.HORIZONTAL:
			dy -= cw

		if r.ysize > (self.ysize+cw):
			for ref in self.references:
				ref.move([0, dy])
		elif r.ysize < self.ysize:
			r = r.move([0, -dy])

		self << r
		self.connect_giant_meander_resistor(r, cw, direction)

	def connect_giant_meander_resistor(self, r, width, direction):
		if direction == Direction.HORIZONTAL:
			self.connect_rect(r.ports["e1_2"], self.pads[1].ports["e2"],layer=self.conlayer_name, w=width, mode='L')
			self.connect_rect(r.ports["e1_1"], self.pads[2].ports["e4"], layer=self.conlayer_name, w=width, mode='L')
		elif direction == Direction.VERTICAL:
			self.connect_rect(r.ports["e1_1"], self.pads[1].ports["e2"],layer=self.conlayer_name, w=width, mode='U')
			self.connect_rect(r.ports["e1_2"], self.pads[0].ports["e2"], layer=self.conlayer_name, w=width, mode='manhattan')

	# straight stuff
	def straight_wire_resistor(self, layer, factor, direction):
		width = self.config.get_min_width(layer)
		pf = lph.to_nm(self.config, self.pitch)
		# many resistors are way too short, so we calculate a better resistance value
		# and return it.
		newr = self.calculate_resistance(layer, self.pitch, width*factor)
		self.R = newr = round(newr[0] if direction == Direction.HORIZONTAL else newr[1])
		name = self.make_name(layer,factor,direction)
		r = StripResistor(self.config, name+"_resistor", layer, width*factor, newr, direction=direction, contact_to=self.conlayer_name)
		x = lph.to_nm(self.config, self.left_lower_corner[0])
		y = lph.to_nm(self.config, self.left_lower_corner[1])
		if direction == Direction.HORIZONTAL:
			x -= lph.value_to_grid(self.config, (r.xsize-pf)/2)
		elif direction == Direction.VERTICAL:
			y -= lph.value_to_grid(self.config, (r.ysize-pf)/2)
		self << r.move([x,y])
		return newr

	def check_can_be_straight(self, layer, factor, direction):
		min_width = self.config.get_min_width(layer)
		ohms = self.calculate_resistance(layer, self.pitch, factor*min_width)
		if direction == Direction.VERTICAL:
			ohms = ohms[1]
		elif direction == Direction.HORIZONTAL:
			ohms = ohms[0]
		else:
			raise Exception("No direction given")

		return (self.minimumr <= ohms)

	# actually generating stuff and putting it together:
	def generate_straight_resistor(self, layer, factor, direction):
		# draw the structure
		ohms = self.straight_wire_resistor(layer, factor, direction) # with adjusted R for too short strips

		if direction == Direction.HORIZONTAL:
			self.connect_pads(Direction.VERTICAL)
		elif direction == Direction.VERTICAL:
			self.connect_pads(Direction.HORIZONTAL)

		self.U = ohms*self.Imax

		if direction == Direction.VERTICAL:
			self.i0p = 2
			self.i1p = 3
			self.u0p = 1
			self.u1p = 4
		elif direction == Direction.HORIZONTAL:
			self.i0p = 1
			self.i1p = 2
			self.u0p = 4
			self.u1p = 3

	def generate_meander_resistor(self, layer, factor, direction):
		width = self.config.get_min_width(layer)
		self.connect_pads(direction)
		r = MeanderResistor(self.config, self.name+"_resistor", layer, width*factor, self.minimumr, direction=direction, contact_to=self.conlayer_name)
		if r.xsize < lph.to_nm(self.config, self.pitch) and r.ysize < lph.to_nm(self.config, self.pitch):
			self.normal_meander_resistor(r, direction)
		else:
			self.giant_meander_resistor(r, direction)

		if direction == Direction.VERTICAL:
			self.i0p = 3
			self.i1p = 4
			self.u0p = 2
			self.u1p = 1
		elif direction == Direction.HORIZONTAL:
			self.i0p = 1
			self.i1p = 4
			self.u0p = 2
			self.u1p = 3

def generate(config):
		layers = config.get_resistor_layers()
		factors = config.get_resistor_factors()
		if layers is None:
			return ret

		ret = []
		for layer in layers:
			for factor in factors:
				for direction in [Direction.HORIZONTAL, Direction.VERTICAL]:
					s = DanubeTestResistor(config, layer, factor, direction)
					ret.append(s)
		return ret

