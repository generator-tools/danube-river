from scipy import constants
from math import sqrt

import gdsfactory as gf
import numpy as np

import phidl.geometry as pg
import phidl.path as pp

from phidl import Device

from DanubeRiver.structure import AbstractDanubeStructure

from librepdk.types import Direction
from librepdk.types import ChannelType
from librepdk.contact import ViaStripe

from librepdk.fet import FET
import librepdk.helper as lph

from DanubeRiver.types import DanubeProbeConfiguration

class DanubeTestMOSFET(AbstractDanubeStructure):
	def __init__(self, config, mtype, dims):
		AbstractDanubeStructure.__init__(self, config, self.make_name(mtype, dims))
		self.init_pads(DanubeProbeConfiguration.KELVIN, with_metal=True)
		self.mtype = mtype
		self.dims = dims
		self.generate_mos(mtype, dims)

	# helper function:
	def make_name(self, mtype, dims):
		ret=''
		if mtype == ChannelType.PMOS:
			ret += "P_"
		elif mtype == ChannelType.NMOS:
			ret += "N_"

		ret += str(int(dims[0]))
		ret += "x"
		ret += "W"
		ret += str(int(dims[1]))
		ret += "/L"
		ret += str(int(dims[2]))

		return ret

	def generate_mos(self, mtype, dims):
		fet = FET(self.config, self.name+"_transistor_", mtype, dims, have_bulk_tap=True)
		x = lph.to_nm(self.config, self.left_lower_corner[0]+(self.pitch-fet.xsize*self.ureg.nm)/2)
		y = lph.to_nm(self.config, self.left_lower_corner[1]+(self.pitch-fet.ysize*self.ureg.nm)/2)
		fet = fet.move([x,y])
		fet.name=self.name+"_transistor"
		self << fet
		rw = lph.to_nm(self.config, self.config.get_min_width(self.conlayer_name))
		# TODO: Quad routing flag!
		#self.connect_quad(self.pads[1].ports["e4"], fet.ports["e3_1"], self.conlayer_name) # connect gate
		#self.connect_quad(self.pads[3].ports["e2"], fet.ports["e1_2"], self.conlayer_name) # connect source
		#self.connect_quad(self.pads[0].ports["e4"], fet.ports["e1_3"], self.conlayer_name) # connect drain
		#self.connect_quad(self.pads[2].ports["e2"], fet.ports["e3_4"], self.conlayer_name) # connect bulk
		self.connect_rect(self.pads[1].ports["e4"], fet.ports["e2_1"], self.conlayer_name, mode='C') # connect gate
		self.connect_rect(self.pads[3].ports["e2"], fet.ports["e1_2"], self.conlayer_name) # connect source
		self.connect_rect(self.pads[0].ports["e4"], fet.ports["e1_3"], self.conlayer_name) # connect drain
		self.connect_rect(self.pads[2].ports["e2"], fet.ports["e3_4"], self.conlayer_name) # connect bulk

def generate(config, fingered=False):
	ret = []
	dimensions = config.get_finger_fet_dimensions() if fingered else config.get_fet_dimensions()
	if dimensions is None:
		return ret
	for dims in dimensions:
		s = DanubeTestMOSFET(config, ChannelType.PMOS, dims)
		ret.append(s)
		s = DanubeTestMOSFET(config, ChannelType.NMOS, dims)
		ret.append(s)
	return ret

