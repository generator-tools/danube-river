from scipy import constants
import gdsfactory as gf
import pint

import librepdk.helper as lph

# The configuration wrapper class
from DanubeRiver.config import DanubeConfig

# Documentation output
from DanubeRiver.documentation import DanubeRiverDocumentation
from DanubeRiver.csvdoc import DanubeRiverCSV

# Structure generators
import DanubeRiver.resistors
import DanubeRiver.caps
import DanubeRiver.fet

class DanubeTestWafer(gf.Component):
	dn = "Danube River"
	config = None
	doc = None
	csv = None

	def __init__(self, config, rows, no_doc, no_csv, index=None):
		top_name = config.get_topcell_name()
		gf.Component.__init__(self, top_name)
		self.config = config
		self.no_doc = no_doc

		die_dimensions = self.config.get_die_dimensions()
		self.die_w = die_dimensions[0]
		self.die_h = die_dimensions[1]

		dd = config.get_die_origin()
		ddx = lph.to_nm(config, dd[0])
		ddy = lph.to_nm(config, dd[1])

		process_name = self.config.get_process_name()
		if process_name is not None:
			self.dn+="("+process_name+")"
		if index is not None:
			self.dn+=" #"+str(index)

		ts = self.config.get_banner_label_size()
		if ts is None:
			ts = self.die_w/len(self.dn)
		else:
			ts = self.die_w/len(self.dn) if ts*len(self.dn) > self.die_w else ts

		ts = lph.to_nm(self.config, ts)

		D = gf.components.die(
			size=(lph.to_nm(config, self.die_w), lph.to_nm(config, self.die_h)),
			street_width=1,
			street_length=1,
			die_name=self.dn,
			text_size=ts,
			text_location="SW",
			layer=config.get_abutment_box(),
			bbox_layer=config.get_outline(),
		)
		Dbbox = gf.components.die_bbox(D, street_width=10)
		ddx += Dbbox.xsize/2
		ddy += Dbbox.ysize/2

		if not no_doc:
			self.doc = DanubeRiverDocumentation(self)

		if not no_csv:
			self.csv = DanubeRiverCSV(self)

		if not self.config.disable_bounding_box():
			self << Dbbox.move([ddx,ddy])
		self.add_rows(rows)

	def write_gds(self, outpath):
		gf.Component.write_gds(self, outpath, unit=self.config.get_dbunit())

	def write_pdf_doc(self, outname):
		if self.doc is not None:
			self.doc.generate_pdf(outname)

	def write_csv(self, outname):
		if self.csv is not None:
			self.csv.generate_csv(outname)

	def add_rows(self, rows):
		# create area
		strarea = gf.Component("structure area", with_uuid=True) # the subcell to arrange all structures in
		# deciding what structure goes on what wafer
		die_dimensions = self.config.get_die_dimensions()
		margins = self.config.get_margins()
		bannersize = self.config.get_banner_label_size()
		spacing = self.config.get_structure_spacing()
		maxw = lph.to_nm(self.config, die_dimensions[0]-margins[0]-margins[1])
		maxh = lph.to_nm(self.config, die_dimensions[1]-margins[2]-margins[3]-bannersize)
		spacing = lph.to_nm(self.config, spacing)
		# placing the stuff
		xpos = ypos = 0
		for row in rows:
			for st in row['r']:
				# place structure
				xp = lph.value_to_grid(self.config, xpos)
				yp = lph.value_to_grid(self.config, ypos+row['h']-st.ysize)
				strarea << st.move([xp, yp])
				# add to documentation
				if self.doc is not None:
					self.doc.add_cell(st,[xp, yp])
				# add to CSV
				if self.csv is not None:
					self.csv.add_cell(st,[xp, yp])
				# calculate next x coordinate and snap to grid
				xpos+=st.xsize+spacing
			xpos = 0
			ypos += row['h']
		# calculate offset
		dd = self.config.get_die_origin()
		ddx = lph.to_nm(self.config, dd[0]+margins[0])
		ddy = lph.to_nm(self.config, dd[1]+bannersize*1.5)
		self << strarea.move([ddx,ddy])

def make_wafers(args_config, no_doc, no_csv):
	config = DanubeConfig(args_config)
	# preparing the structures
	structures = []
	if config.have_resistors():
		ar = DanubeRiver.resistors.generate(config)
		if ar is not None:
			structures+=ar
	if config.have_capacitors():
		ar = DanubeRiver.caps.generate(config)
		if ar is not None:
			structures+=ar
	if config.have_mosfets():
		ar = DanubeRiver.fet.generate(config)
		if ar is not None:
			structures+=ar
	if config.have_fingered_mosfets():
		ar = DanubeRiver.fet.generate(config, fingered=True)
		if ar is not None:
			structures+=ar
	def sortStructure(val):
		return val.xsize*val.ysize
	structures.sort(key=sortStructure,reverse=True)

	# deciding what structure goes on what wafer
	die_dimensions = config.get_die_dimensions()
	margins = config.get_margins()
	bannersize = config.get_banner_label_size()
	maxw = lph.to_nm(config, die_dimensions[0]-margins[0]-margins[1])
	maxh = lph.to_nm(config, die_dimensions[1]-margins[2]-margins[3]-bannersize*1.5)

	# sorting the structures into rows
	w = h = 0
	rows = []
	r = []
	spacing = lph.to_nm(config, config.get_structure_spacing())
	for i, st in enumerate(structures):
		r.append(st)
		w += st.xsize+spacing
		h = st.ysize if st.ysize>h else h
		next_w = structures[i+1].xsize if i+1<len(structures) else structures[i].xsize
		if i+1==len(structures) or w+next_w+spacing>maxw:
			h += spacing
			rows.append({'r':r, 'w':w, 'h':h})
			r = []
			h = w = 0
	have_index = sum(r['h'] for r in rows) > maxh

	# organize
	ret=[]
	structures = []
	h = 0
	cnt = 0
	for i, r in enumerate(rows):
		structures.append(r)
		h += r['h']
		next_h = rows[i+1]['h'] if i+1 < len(rows) else rows[i]['h']
		if h+next_h>=maxh or i+1==len(rows):
			cnt = cnt+1 if have_index else None
			ret.append(DanubeTestWafer(config, structures, no_doc, no_csv, index=cnt)) # create wafer
			# reset
			structures.clear()
			h = 0
	return ret
