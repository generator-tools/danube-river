import configparser

from librepdk.config import Config
from librepdk.config import available_technologies

class DanubeConfig(Config):
	config = None
	cfgfile = None

	def __init__(self, cfgfile):
		self.config = configparser.ConfigParser()
		self.config.read(cfgfile)
		self.cfgfile = cfgfile
		process = self.get_process()
		assert(process is not None)
		assert(process in available_technologies())
		super(DanubeConfig, self).__init__(process)

	def get_process(self):
		l = self.get_section_value('process', 'name')
		return l

	def get_lambda(self):
		l = self.get_section_value_unit('process', 'lambda')
		return l

	def get_structure_spacing(self):
		s = self.get_section_value_unit('die', 'spacing')
		return 0 if s is None else s

	def get_margins(self):
		ret = []
		for i, n in enumerate(['left','right','top','bottom']):
			m = self.get_section_value_unit('die', n+'margin')
			ret.append(0 if m is None else m)
		return ret

	def disable_bounding_box(self):
		res = self.get_section_value('die', 'disableboundingbox')
		if res is not None:
			res = res.strip()
		return (res == 'true')

	def get_structue_label_size(self):
		l = self.get_section_value_unit('die', 'labelsize')
		return l

	def get_pad_label_size(self):
		l = self.get_section_value_unit('pad', 'labelsize')
		return l

	def get_banner_label_size(self):
		l = self.get_section_value_unit('die', 'bannersize')
		return l

	def get_topcell_name(self):
		n = self.get_section_value('die', 'name')
		return 'top' if n is None else n

	# layers which can be overwritten in the config: pads, pad labels, structure labels
	def get_pad_layer(self):
		return self.get_section_value('pad', 'layer')

	def get_structure_label_layer(self):
		l = self.get_section_value('die', 'labellayer')
		if l is None:
			return self.get_pad_layer()
		else:
			return l

	def get_pad_label_layer(self):
		l = self.get_section_value('pad', 'labellayer')
		if l is None:
			return self.get_structure_label_layer()
		else:
			return l

	def get_pad_connection_layer(self):
		l = self.get_section_value('pad', 'connectlayer')
		if l is None:
			return self.get_pad_layer()
		else:
			return l

	def get_section(self, sname):
		for section in self.config.sections():
			if section.lower() == sname:
				return self.config[section]
		return None

	def get_section_value(self, sname, vname):
		ret = None
		section = self.get_section(sname)
		if section is None:
			return ret

		for v in section:
			if v.lower() == vname:
				ret = section[v]

		return ret
	
	def get_section_value_unit(self, sname, vname):
		v = self.get_section_value(sname, vname)
		return None if v is None else self.ureg.Quantity(v)

	def get_die_origin(self):
		v = self.get_section_value_unit('die', 'originx')
		x = 0 if v is None else v
		v = self.get_section_value_unit('die', 'originy')
		y = 0 if v is None else v
		return (x,y)

	def get_die_dimensions(self):
		dw = self.get_section_value_unit('die', 'width')
		dh = self.get_section_value_unit('die', 'height')
		return (dw, dh)

	def get_pad_pitch(self):
		r = self.get_section_value_unit('pad', 'pitch')
		return r

	def get_pad_dimensions(self):
		dw = self.get_section_value_unit('pad', 'width')
		dh = self.get_section_value_unit('pad', 'height')
		return (dw, dh)

	def get_capacitor_layer_pairs(self):
		ret=[]
		layer_pairs = self.get_section_value('capacitor', 'layerpairs')
		if layer_pairs is None:
			return ret

		layer_pairs = layer_pairs.split(',')
		for lp in layer_pairs:
			lp = lp.split(':')
			lp = (lp[0].strip(),lp[1].strip())
			ret.append(lp)

		return ret

	def get_resistor_layers(self):
		ret=[]
		layers = self.get_section_value('resistor', 'layers')
		if layers is None:
			return ret

		layers = layers.split(',')
		for i in layers:
			ret.append(i.strip())

		return ret

	def get_resistor_factors(self):
		ret=[]
		factors = self.get_section_value('resistor', 'factors')
		if factors is None:
			return ret

		factors = factors.strip().split(',')
		for i in factors:
			ret.append(float(i))

		return ret

	def get_resistor_ohm(self, name):
		ohm = self.get_section_value_unit('resistor', name)
		return ohm

	def get_resistor_minimum_ohm(self):
		ohm = self.get_resistor_ohm('minimumohm')
		if ohm is None:
			return 10.0
		return ohm

	def get_fet_dimensions(self):
		ret=[]
		dimensions = self.get_section_value('mosfet', 'dimensions')
		if dimensions is None:
			return ret
		dimensions = dimensions.strip().split(',')
		for i in dimensions:
			i = i.split(':')
			w = float(i[0])
			h = float(i[1])
			ret.append((1,w,h))
		return ret

	def get_finger_fet_dimensions(self):
		ret=[]
		dimensions = self.get_section_value('mosfet', 'fingerdimensions')
		if dimensions is None:
			return ret
		dimensions = dimensions.strip().split(',')
		for i in dimensions:
			i = i.split('x')
			n = int(i[0])
			if len(i) > 1:
				i = i[1].split(':')
				w = float(i[0])
				h = float(i[1])
				ret.append((n,w,h))
		return ret

	def get_resistor_max_current(self):
		i = self.get_section_value_unit('resistor', 'maxcurrent')
		return i

	def have_resistors(self):
		res = self.get_section_value('flags', 'resistance')
		if res is not None:
			res = res.strip()
		return (res == 'true')

	def have_capacitors(self):
		res = self.get_section_value('flags', 'capacitance')
		if res is not None:
			res = res.strip()
		return (res == 'true')

	def have_mosfets(self):
		res = self.get_section_value('flags', 'mosfet')
		if res is not None:
			res = res.strip()
		return (res == 'true')

	def have_fingered_mosfets(self):
		res = self.get_section_value('flags', 'fingeredmosfet')
		if res is not None:
			res = res.strip()
		return (res == 'true')

	def have_power_mosfets(self):
		res = self.get_section_value('flags', 'powermosfet')
		if res is not None:
			res = res.strip()
		return (res == 'true')
