import gdsfactory as gf

from librepdk.contact import ViaStripe

from DanubeRiver.structure import AbstractDanubeStructure
from DanubeRiver.types import DanubeProbeConfiguration

import librepdk.helper as lph
import librepdk.contact as lpc

class DanubeTestCapacitor(AbstractDanubeStructure):
	debug = False
	layer_pair = None
	area = None

	def __init__(self, config, layer_pair):
		AbstractDanubeStructure.__init__(self, config, self.make_name(layer_pair))
		self.layer_pair = layer_pair
		self.init_pads(DanubeProbeConfiguration.KELVIN, with_metal=True)
		self.area = self.capacitor_cross(layer_pair)*(self.ureg.nm**2) # it's all in nano meters

	# helper function:
	def make_name(self, layer_pair):
		ret = layer_pair[0].upper()
		ret += "/"
		ret += layer_pair[1].upper()
		ret += "_CAP"
		return ret

	def create_cap_polygon(self, name, layer):
		cr = gf.Component(name+"_POLY_"+layer)
		sp = self.config.get_min_spacing((layer,layer))*2
		ps = lpc.get_min_viagroup_pad_size(self.config, layer, self.conlayer_name)
		tlsp = self.config.get_min_spacing((self.conlayer_name,self.conlayer_name))
		spps = sp + ps
		lid = self.config.get_layer_id(layer) # id of layer

		padw = self.pad_w
		padw -= ps
		padh = self.pad_h
		padh -= ps

		pts = []

		x = y = 0
		pts.append([x,y]) # pt1
		x += self.pitch
		pts.append([x,y]) # pt2
		y += padh
		pts.append([x,y]) # pt3
		x += padw
		pts.append([x,y]) # pt4
		y += self.pitch
		pts.append([x,y]) # pt5
		x -= padw
		pts.append([x,y]) # pt6
		y += padh
		pts.append([x,y]) # pt7
		x -= self.pitch
		pts.append([x,y]) # pt8
		y -= padh
		pts.append([x,y]) # pt9
		x -= padw
		pts.append([x,y]) # pt10
		y -= self.pitch
		pts.append([x,y]) # pt11
		x += padw
		pts.append([x,y]) # pt12

		# setting offsets (less confusing that way)
		pts[3][0]-=spps
		pts[4][0]-=spps
		pts[4][1]-=sp
		pts[5][0]-=sp
		pts[5][1]-=sp
		pts[6][0]-=sp
		pts[6][1]-=spps
		pts[7][0]+=sp
		pts[7][1]-=spps
		pts[8][0]+=sp
		pts[8][1]-=tlsp
		pts[9][1]-=tlsp

		# convert the stuff into a polygon
		xfpts = []
		yfpts = []
		cnt = 1
		for p in pts:
			x = 0 if p[0]==0 else lph.to_nm(self.config, p[0])
			y = 0 if p[1]==0 else lph.to_nm(self.config, p[1])
			xfpts.append(x)
			yfpts.append(y)
			# labeling the corners for debugging
			if self.debug:
				pl = gf.components.text(text=str(cnt)+"("+str(x)+","+str(y)+")", size=1000, justify="center", layer=self.padnumlayer)
				cr << pl.move([x, y])
				cnt+=1

		cr.add_polygon([xfpts, yfpts], layer = lid)

		return cr.move([-cr.xmin,-cr.ymin])

	def capacitor_cross_1(self, name, layer):
		cr = gf.Component(name) # cross on specific layer
		lid = self.config.get_layer_id(layer) # id of layer
		spacing = self.config.get_min_spacing((layer,layer))
		tlsp = self.config.get_min_spacing((self.conlayer_name,self.conlayer_name))
		padw = self.pad_w
		padh = self.pad_h
		ps = lpc.get_min_viagroup_pad_size(self.config, layer, self.conlayer_name)

		# via stripes
		via_name = name+'_CONTACT'
		con1 = ViaStripe(self.config, via_name+'1', layer, ps, self.pitch-tlsp, self.conlayer_name, less_vias=True)
		con2 = ViaStripe(self.config, via_name+'2', layer, ps, self.pitch, self.conlayer_name, less_vias=True)

		# polycross
		pol = self.create_cap_polygon(name, layer)
		cr << pol.move(lph.snap_to_grid(self.config, [con2.xsize,con1.xsize]))

		#----------------------------------
		cx = 0
		cy = lph.to_nm(self.config, padh)
		con1 = con1.move([cx,cy])
		cr << con1
		#----------------------------------
		con2 = con2.rotate(-90)
		cx = padw
		cx = lph.to_nm(self.config, cx)
		cy = con2.ysize*self.ureg.nm
		cy = lph.to_nm(self.config, cy)
		con2 = con2.move([cx,cy])
		cr << con2
		#----------------------------------

		return cr

	def capacitor_cross(self, layer_pair):
		name = self.make_name(layer_pair)
		padw = self.pad_w
		padh = self.pad_h

		x = self.left_lower_corner[0]-self.pad_w
		y = self.left_lower_corner[1]-self.pad_h

		cr1 = self.capacitor_cross_1(name+'_'+layer_pair[0], layer_pair[0])
		cr2 = self.capacitor_cross_1(name+'_'+layer_pair[1], layer_pair[1])

		cr2 = cr2.rotate(180)

		x1 = lph.to_nm(self.config, x)
		y1 = lph.to_nm(self.config, y)
		cr1 = cr1.move([x1,y1])
		self << cr1

		x2 = lph.to_nm(self.config, x+self.pitch+2*self.pad_w)
		y2 = lph.to_nm(self.config, y+self.pitch+2*self.pad_h)
		cr2 = cr2.move([x2,y2])
		self << cr2

		cr = gf.geometry.boolean(A=cr1, B=cr2, operation="and", layer=(0, 0))
		return cr.area()

def generate(config):
	ret = []
	layerpairs = config.get_capacitor_layer_pairs()
	if layerpairs is not None:
		for lp in layerpairs:
			cap = DanubeTestCapacitor(config, lp)
			ret.append(cap)
	return ret
