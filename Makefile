# Generating all:
all:
	bin/danube_generate -c configs/sky130.cfg -o tapeout/sky130.gds
	bin/danube_generate -c configs/gf180.cfg -o tapeout/gf180.gds
	bin/danube_generate -c configs/gf180-large.cfg -o tapeout/gf180-large.gds
	bin/danube_generate -c configs/kacst-ls1u.cfg -o tapeout/kacst-ls1u.gds

# For faster speed we can also not generate the documentation:
nd:
	bin/danube_generate -nd -c configs/sky130.cfg -o tapeout/sky130.gds
	bin/danube_generate -nd -c configs/gf180.cfg -o tapeout/gf180.gds
	bin/danube_generate -nd -c configs/gf180-large.cfg -o tapeout/gf180-large.gds
	bin/danube_generate -nd -c configs/kacst-ls1u.cfg -o tapeout/kacst-ls1u.gds


gf180:
	bin/danube_generate -nd -c configs/gf180.cfg -o tapeout/gf180.gds
	cp tapeout/gf180.gds tapeout/caravel_user_project/gf180_teststructures/gf180_teststructures.gds 


