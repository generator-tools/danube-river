# Danube River: A test wafer generator

The idea of Danube is to provide a collection of test structure generators,
which spit out specific test cases for characterizing a new process.

The generation happens by executing a shell command which then loads the
configuration file provided, generates the test structures based on the
parameters in the configuration file and then puts it onto a waver with
the dimensions provided in said configuration file.

## Motivation

The Test Chip should make it possible to correctly measure all important physical properties for building correct simulation models in SPICE for electric circuics on a silicon wafer.
Besides the basic values like resistances and capacitance of the metal layers also values for the active parts like diodes, transistors and others.
Another important aspect are the properties and side-effects caused by machines that which are related to the orientation of the elements on the wafer, which are playing an important role.
These effects, especially on the edges of layers, should be handled by special measurements.

## Cloning the repository

Clone the repository recursively with its submodules

    git clone --recurse-submodules https://gitlab.libresilicon.com/generator-tools/danube-river.git

## Requirements

This script and its libraries NEED Python 3.10 or later!!!

On Debian/Ubuntu you will also need to install libcairo2-dev cmake and build-essential

## Installation
If on Debian, first install the requirements

    sudo apt-get install libcairo2-dev cmake build-essential python3.10-dev python3.10 python3-distutils python3-setuptools python3.10-full

run

    pip3 install -e .

### Configuration
The configuration for various test wafers can be found in the config-subfolder.
In the configuration file, you can enable and disable process features to be tested and define for what
process you wish to generate a test wafer for.

New processes should be added in the LibrePDK (librepdk/technologies), which is included as a submodule in
Danube.

In case you're using Danube for characterizing your own new process, just will in ball park numbers from your
predictions based on your process parameters in there, in order to get started.

Just copy sky130.py and edit the values.

### Example
The command:

    danube_generate -c $DANUBE_REPO/configs/kacst-ls1u.cfg -o danube.gds

Generates a GDSII file with the name danube.gds as well as a PDF file named danube.pdf and a CSV file,
which gives a detailed overview over all the test structures generated.

## Measurements

For the sizing of all the test-structures, logarithmic separation is very suitable.
Which means.

1. minimal-size
2. minimal-size * 2e0
3. minimal-size * 5e0
4. minimal-size * 1e1
5. minimal-size * 2e1
6. minimal-size * 5e1
7. minimal-size * 1e2

Most test-structures are provided with 4 pads to support a 4-terminal sensing interface.
For resistances, this arrangement is known als "Kelvin connection".

For the SkyWater 130 process test wafer the pads for the nail-probers are 40x40 micron in size, the spacing between the pads is 60 microns,
this can however be changed in the configuration file configs/default.cfg

### (Sheet) Resistance

The electrical resistance of the conductive materials on the wafer can be calculated with the following formula:

$R = \frac{\rho}{thickness} \frac{length}{width} $

$\rho$ is a material constant of the conductor.
Due to the reason that the layer thickness of the conductor that is to be measured is approximately uniform on the whole wafer, we can rewrite the formula to

$R = R_{\square} \frac{length}{width}$

whereby

$R_{\square} = \frac{\rho}{thickness}$.

is usually called as "Sheet Resistance" with the unit

$[R_{\square}] = \frac{\Omega}{square}$.

The sheet resistance of the following layers is of special importance:

1. NWELL
2. PWELL
3. NBASE
4. PBASE
5. RPOLY
6. PDCONTACT
7. METAL1
8. METAL2
9. METAL3

Besides the minimum track width also wide tracks and meander-form test structures for very long lengths are interesting.
All test structures for the Sheet Resistance measurement are designed for $100 R_{\square}$.

### (Contact) Resistance

Contacts (Vias) between different conducting layers are primarily designated by their resistance.
Therefore special test-structures for contacts of different forms and sizes are required - going from the minimal size to a (small) multiple of the minimal size.

1. PDCONTACT
2. VIA
3. VIA2

### Capacity

The electrical capacity of conducting materials on a wafer can be modeled with the formula

$C = \frac{\varepsilon}{distance} width \cdot length $

$\varepsilon$ is again the material constant of the isolator between the conductors.

The capacity is especially interesting between the following layers:

1. POLYSI <-> NWELL
2. POLYSI <-> PWELL
3. METAL1 <-> NWELL
4. METAL1 <-> PWELL
5. METAL2 <-> METAL1
6. METAL3 <-> METAL2

but also the capactity between areas on the following layers:

1. POLYSI <-> POLYSI
2. METAL1 <-> METAL1
3. METAL2 <-> METAL2
4. METAL3 <-> METAL3

For the last ones special test structures are designed (both for minimal size but also for minimal size against the so called "Big Metal" with > 10 $ \lambda $).
The distance between conducting areas is being varied to better measure cross-talk.

### Circuit Elements

1. Diode
2. Z-Diode
2. p-channel enhancement MOSFET
3. n-channel enhancement MOSFET
4. p-channel enhancement MOSFET with Guard Ring
5. n-channel enhancement MOSFET with Guard Ring

Besides that the following circuits might be interesting:

6. npn BJT
7. pnp BJT
8. npn BJT with Guard Ring
9. pnp BJT with Guard Ring

## Die

All test structures should, if possible, be placed multiple times in 90 degrees rotated orientations on the wafer.
All test stuctures should, if possible, be placed both in the middle and at the edge of a wafer.
