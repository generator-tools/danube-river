from setuptools import setup

name='DanubeRiver'
version='0.1'
release=version+'.0'

setup(
	name=name,
	version=version,
	author='A test wafer generator',
	author_email='leviathan@libresilicon.com',
	packages=['DanubeRiver'],
	scripts=['bin/danube_generate'],
	data_files=[('etc',['configs/default.cfg'])],
	url='https://redmine.libresilicon.com/projects/danube-river',
	license='LICENSE.txt',
	description='A tool for generating test wafer layout for characterizing new manufacturing processes.',
	long_description=open('README.md').read(),
	install_requires=[
		"configparser",
		"argparse",
		"gdsfactory",
		"scipy",
		"phidl",
		"numpy",
		"lclayout",
		"pylatex",
		"pycairo",
		"cairosvg",
		"pathlib",
		"pint",
	],
	command_options={
		'build_sphinx': {
			'project': ('setup.py', name),
			'version': ('setup.py', version),
			'release': ('setup.py', release),
			'source_dir': ('setup.py', 'docs')
		}
	},
)
